## Breast Cancer Classification Model

This program implements a machine learning model to classify breast cancer data as benign or malignant using logistic regression and k nearest neighbor algorithms.

Dataset Source: https://archive.ics.uci.edu/ml/datasets/breast+cancer+wisconsin+(diagnostic)